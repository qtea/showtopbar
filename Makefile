NAME=showtopbar
DOMAIN=teatwig.net

.PHONY: all tscompile pack install clean

all: $(NAME).zip

node_modules: package.json
	pnpm install

tscompile: node_modules
	pnpm run compile

schemas/gschemas.compiled:
	@mkdir -p dist/schemas/
	glib-compile-schemas --strict src/schemas/ --targetdir dist/schemas/

$(NAME).zip: tscompile schemas/gschemas.compiled
	@cp COPYING.md dist/
	@cp src/metadata.json dist/
	@cp -r src/schemas dist/
	@cp src/Settings.ui dist/
	@(cd dist && zip ../$(NAME).zip -9r .)

pack: $(NAME).zip

install: $(NAME).zip
	@touch ~/.local/share/gnome-shell/extensions/$(NAME)@$(DOMAIN)
	@rm -rf ~/.local/share/gnome-shell/extensions/$(NAME)@$(DOMAIN)
	@cp -r dist ~/.local/share/gnome-shell/extensions/$(NAME)@$(DOMAIN)

clean:
	rm -rf dist node_modules $(NAME).zip schemas/gschemas.compiled
