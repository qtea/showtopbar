# Show Top Bar

Normally Gnome hides the panel whenever a window is in fullscreen, even if it is not the focused window.

This extension always makes the panel visible unless the focused window is in fullscreen.

This extension is based on [Hide Top Bar](https://gitlab.gnome.org/tuxor1337/hidetopbar) (which in turn uses code from [Dash to Dock](https://github.com/micheleg/dash-to-dock)) since it offered a good starting point for the panel hiding/showing logic.
Because this extension focuses only on showing/hiding the panel dependent on the state of windows and not their location a lot of the code was either removed or heavily altered.

## Build

Build a ZIP file containing the extension:
```sh
make
```

Or install it directly to `~/.local/share/gnome-shell/extensions`:
```sh
make install
```

## License

Copyright (c) 2024 tea☆

Copyright (c) 2013-2023 Thomas Vogt.

Copyright (c) 2012-2013 Mathieu Lutfy.

Copyright (c) 2012 Philip Witte.

Show Top Bar is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Show Top Bar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Show Top Bar (see [COPYING.md](./COPYING.md)).
If not, see <https://www.gnu.org/licenses/>.
