/**
 * This file is part of Show Top Bar
 *
 * Copyright (C) 2020 Thomas Vogt
 * Copyright (C) 2024 tea☆
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Clutter from 'gi://Clutter';
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';
import * as Config from 'resource:///org/gnome/shell/misc/config.js';
import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import {DEBUG, GlobalSignalsHandler} from './convenience.js';
import {OverlapChecker, PanelStatus} from './overlapChecker.js';

const [major] = Config.PACKAGE_VERSION.split('.');
const shellVersion = Number.parseInt(major);

const PanelBox = Main.layoutManager.panelBox;
const _searchEntryBin = Main.overview._overview._controls._searchEntryBin;

export class PanelVisibilityManager {
  private base_y: number;
  private signalsHandler: GlobalSignalsHandler;
  private overlapChecker: OverlapChecker;
  private animationActive = false;
  private bindTimeoutId: number;
  private panelAreaBlocker?: Clutter.Actor;

  constructor(private settings: Gio.Settings) {
    this.base_y = PanelBox.y;
    this.signalsHandler = new GlobalSignalsHandler();

    this.overlapChecker = new OverlapChecker();

    this.addOrUpdatePanelAreaBlocker();
    // TODO do we still need this as a timeout?
    this.bindTimeoutId = GLib.timeout_add(
      GLib.PRIORITY_DEFAULT,
      100,
      this.bindUIChanges.bind(this)
    );
  }

  /**
   * Used to show or hide the panel depending on the status returned
   * by the overlap checker (as long as the overview isn't visible).
   */
  private toggleVisibility(animate: boolean, trigger: string): void {
    if (Main.overview.visible) {
      return;
    }

    if (this.overlapChecker.panelStatus === PanelStatus.HIDDEN) {
      this.hide(animate, trigger);
    } else {
      this.show(animate, trigger);
    }
  }

  private hide(animate: boolean, trigger: string): void {
    DEBUG('hide(' + trigger + ')');

    if (this.animationActive) {
      PanelBox.remove_all_transitions();
      this.animationActive = false;
    }

    const hiddenY = this.base_y - PanelBox.height;

    if (!animate) {
      PanelBox.y = hiddenY;
    } else {
      this.animationActive = true;
      const animationTime = this.settings.get_double('animation-time-autohide');
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (PanelBox as any).ease({
        y: hiddenY,
        duration: animationTime,
        mode: Clutter.AnimationMode.EASE_OUT_QUAD,
        onComplete: () => {
          this.animationActive = false;
          PanelBox.hide();
        },
      });
    }
  }

  private show(animate: boolean, trigger: string): void {
    DEBUG('show(' + trigger + ')');

    if (this.animationActive) {
      PanelBox.remove_all_transitions();
      this.animationActive = false;
    }

    PanelBox.show();
    if (!animate) {
      PanelBox.y = this.base_y;
    } else {
      this.animationActive = true;
      const animationTime = this.settings.get_double('animation-time-autohide');
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (PanelBox as any).ease({
        y: this.base_y,
        duration: animationTime,
        mode: Clutter.AnimationMode.EASE_OUT_QUAD,
        onComplete: () => {
          this.animationActive = false;
          this.overlapChecker.check();
        },
      });
    }
  }

  /**
   * Windows seem to have some kind of snap to edges, which means two windows with the same y (right under panel) may behave differently when the panel is shown.
   * A window that is just placed there will stay there when the panel is hidden and shown again.
   * A window that was dragged into the panel will move up (y=0) when the panel is hidden, which means the panel will push it down when it is shown again.
   * To avoid this unwanted movement of the window we add an actor that always takes up the space of the panel and affects the struts.
   */
  private addOrUpdatePanelAreaBlocker(): void {
    // TODO no idea if this actually works when the primary monitor changes
    if (this.panelAreaBlocker) {
      Main.layoutManager.removeChrome(this.panelAreaBlocker);
    }

    // the actor will be above the panel but does not prevent interaction with it
    // set `color` to make it visible
    this.panelAreaBlocker = new Clutter.Actor({
      x: PanelBox.x,
      y: PanelBox.y,
      width: PanelBox.width,
      height: PanelBox.height,
    });
    Main.layoutManager.addChrome(this.panelAreaBlocker, {
      affectsStruts: true,
      // hide when a fullscreen window is visible
      // (shouldn't matter since the actor doesn't prevent interaction anyway)
      trackFullscreen: true,
    });
  }

  private updateSearchEntryPadding(): void {
    if (!_searchEntryBin) {
      return;
    }
    const scale = Main.layoutManager.primaryMonitor!.geometryScale;
    const offset = PanelBox.height / scale;
    _searchEntryBin.set_style(`padding-top: ${offset}px;`);
  }

  private bindUIChanges(): boolean {
    // TODO the panel is flickering when leaving overview and a fullscreen window is in the background

    // never animate when showing or hiding the overview because the panel is either visible or should be hidden right away because a window is fullscreen
    // overview signals: https://gitlab.gnome.org/GNOME/gnome-shell/-/blob/46.1/js/ui/overview.js?ref_type=tags#L121-140
    this.signalsHandler.add(Main.overview, 'showing', () =>
      this.show(false, 'showing-overview')
    );
    this.signalsHandler.add(Main.overview, 'hiding', () =>
      this.toggleVisibility(false, 'hiding-overview')
    );
    // without this signal the panel does not show when returning from overview
    this.signalsHandler.add(Main.overview, 'hidden', () =>
      this.toggleVisibility(false, 'overview-hidden')
    );
    this.signalsHandler.add(PanelBox, 'notify::height', () =>
      this.updateSearchEntryPadding()
    );
    this.signalsHandler.add(Main.layoutManager, 'monitors-changed', () => {
      this.addOrUpdatePanelAreaBlocker();
      this.overlapChecker.check();
    });
    this.overlapChecker.connect('status-changed', () => {
      this.toggleVisibility(true, 'overlap-checker');
      // true to indicate that the signal was handled, doesn't really matter since we only have one subscriber
      return true;
    });

    this.bindTimeoutId = 0;
    // glib timeout gets destroyed when we return false
    return false;
  }

  public destroy(): void {
    if (this.bindTimeoutId) {
      GLib.source_remove(this.bindTimeoutId);
      this.bindTimeoutId = 0;
    }
    this.overlapChecker.destroy();
    this.signalsHandler.destroy();
    this.panelAreaBlocker?.destroy();
    if (_searchEntryBin) {
      _searchEntryBin.style = null;
    }

    this.show(false, 'destroy');
  }
}
