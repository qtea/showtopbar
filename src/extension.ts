/**
 * This file is part of Show Top Bar
 *
 * Copyright (C) 2020 Thomas Vogt
 * Copyright (C) 2024 tea☆
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {ExtensionMetadata} from '@girs/gnome-shell/extensions/extension';
import Gio from 'gi://Gio';
import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';
import {DEBUG} from './convenience.js';
import {PanelVisibilityManager} from './panelVisibilityManager.js';

export default class ShowtopbarExtension extends Extension {
  private mSettings?: Gio.Settings;
  private mPVManager?: PanelVisibilityManager;

  constructor(metadata: ExtensionMetadata) {
    super(metadata);
    console.log(`Initiating ${this.uuid}`);
  }

  enable(): void {
    DEBUG('enable()');
    this.mSettings = this.getSettings();
    this.mPVManager = new PanelVisibilityManager(this.mSettings);
  }

  disable(): void {
    DEBUG('disable()');
    this.mPVManager?.destroy();

    this.mPVManager = undefined;
    this.mSettings = undefined;
  }
}
