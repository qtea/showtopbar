/**
 * This file is part of Show Top Bar
 *
 * Copyright (C) 2024 tea☆
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Note that the code in this file is based on code from the Dash to Dock
// Gnome Shell extension (https://github.com/micheleg/dash-to-dock) but
// most of it has been modified and adapted to fit the specific use case.
// Dash to Dock is distributed under the terms of the
// GNU General Public License, version 2 or later.

import GLib from 'gi://GLib';
import Meta from 'gi://Meta';
import Shell from 'gi://Shell';
import {EventEmitter} from 'resource:///org/gnome/shell/misc/signals.js';
import {GlobalSignalsHandler} from './convenience.js';

// A good compromise between reactivity and efficiency; to be tuned.
export const OVERLAP_CHECK_INTERVAL = 100;

export enum PanelStatus {
  VISIBLE,
  HIDDEN,
}

// List of windows type taken into account.
// Order is important (keep the original enum order).
export const handledWindowTypes = [
  Meta.WindowType.NORMAL,
  Meta.WindowType.DOCK,
  Meta.WindowType.DIALOG,
  Meta.WindowType.MODAL_DIALOG,
  Meta.WindowType.TOOLBAR,
  Meta.WindowType.MENU,
  Meta.WindowType.UTILITY,
  Meta.WindowType.SPLASHSCREEN,
];

interface OverlapCheckerSignals {
  'status-changed': [PanelStatus];
}

/**
 * Decides whether a focused window overlaps the panel.
 * Generally the panel is hidden when:
 * - the focused window is fullscreen (focus on panel monitor)
 * - the topmost window is fullscreen (focus not on panel monitor)
 * OverlapChecker object: emit 'status-changed' signal when the calculated overlap of windows with the panel changes.
 */
export class OverlapChecker extends EventEmitter<OverlapCheckerSignals> {
  private signalsHandler: GlobalSignalsHandler;
  private windowTracker: Shell.WindowTracker;

  private _panelStatus: PanelStatus;
  private checkOverlapTimeoutContinue: boolean;
  private checkOverlapTimeoutId: number;
  private trackedWindows: Map<Meta.WindowActor, number>;

  constructor() {
    super();

    this.signalsHandler = new GlobalSignalsHandler();
    this.windowTracker = Shell.WindowTracker.get_default();

    this._panelStatus = PanelStatus.VISIBLE;

    this.checkOverlapTimeoutContinue = false;
    this.checkOverlapTimeoutId = 0;

    this.trackedWindows = new Map();

    this.connectGlobalSignals();
    this.addSignalsForExistingWindows();
    this.doCheck();
  }

  public get panelStatus(): PanelStatus {
    return this._panelStatus;
  }

  private connectGlobalSignals(): void {
    // add signals on windows created from now on
    this.signalsHandler.add(
      global.display,
      // https://mutter.gnome.org/meta/signal.Display.window-created.html
      'window-created',
      (self: Meta.Display, object: Meta.Window) =>
        this.windowCreated(self, object)
    );

    // triggered for instance when the window list order changes,
    // including when the workspace is switched
    this.signalsHandler.add(
      global.display,
      // https://mutter.gnome.org/meta/signal.Display.restacked.html
      'restacked',
      () => this.check()
    );

    // when windows are always on top, the focus window can change
    // without the windows being restacked. Thus monitor window focus
    // change.
    this.signalsHandler.add(
      this.windowTracker,
      // uses notify_by_pspec since "focus-app" is a param_spec on windowTracker
      // https://gitlab.gnome.org/GNOME/gnome-shell/-/blob/46.1/src/shell-window-tracker.c?ref_type=tags#L773
      'notify::focus-app',
      () => this.check()
    );

    // updates when monitor changes, for instance in multimonitor, when
    // monitors are attached
    this.signalsHandler.add(
      global.backend.get_monitor_manager(),
      // https://mutter.gnome.org/meta/signal.MonitorManager.monitors-changed.html
      'monitors-changed',
      () => this.check()
    );
  }

  private addSignalsForExistingWindows(): void {
    global.get_window_actors().forEach(wa => {
      this.addWindowSignals(wa);
    }, this);
  }

  private windowCreated(display: Meta.Display, metaWindow: Meta.Window): void {
    this.addWindowSignals(
      metaWindow.get_compositor_private() as Meta.WindowActor
    );
    this.doCheck();
  }

  private addWindowSignals(wa: Meta.WindowActor): void {
    if (!this.handledWindow(wa)) {
      return;
    }
    const signalId = wa.connect('notify::allocation', this.check.bind(this));
    this.trackedWindows.set(wa, signalId);
    wa.connect('destroy', this.removeWindowSignals.bind(this));
  }

  private removeWindowSignals(wa: Meta.WindowActor): void {
    const signalId = this.trackedWindows.get(wa);
    if (signalId) {
      wa.disconnect(signalId);
      this.trackedWindows.delete(wa);
    }
  }

  public check(): void {
    /* Limit the number of calls to the doCheck function */
    if (this.checkOverlapTimeoutId > 0) {
      this.checkOverlapTimeoutContinue = true;
      return;
    }

    this.doCheck();

    this.checkOverlapTimeoutId = GLib.timeout_add(
      GLib.PRIORITY_DEFAULT,
      OVERLAP_CHECK_INTERVAL,
      (): boolean => {
        this.doCheck();
        if (this.checkOverlapTimeoutContinue) {
          this.checkOverlapTimeoutContinue = false;
          return GLib.SOURCE_CONTINUE;
        } else {
          this.checkOverlapTimeoutId = 0;
          return GLib.SOURCE_REMOVE;
        }
      }
    );
  }

  private doCheck(): void {
    let panelStatus = PanelStatus.VISIBLE;

    /*
     * `windows` are ordered in stacking order
     * `above` (always on top) windows are always at the end
     * `global.display.focusWindow` is the actually focused window (even if blocked by an always on top one)
     */

    const focusWindow = global.display.focusWindow;
    if (
      focusWindow &&
      focusWindow.is_on_primary_monitor() &&
      focusWindow.is_fullscreen()
    ) {
      // if the focus window is fullscreen and on the panel monitor always hide the panel, even if there's an always on top window
      // this means always on top fullscreen windows won't hide the panel if the focus is somewhere else, but that seems sensible
      // TODO: maybe check if window_type===handledWindowTypes?
      panelStatus = PanelStatus.HIDDEN;
    } else {
      // otherwise check if a fullscreen window is the topmost one on the panel monitor
      const windows = this.getHandledWindowsOnPanelMonitor().filter(wa => {
        const metaWindow = wa.get_meta_window()!;
        // remove always on top windows *unless* it's the focused window
        return metaWindow === focusWindow || !metaWindow.is_above();
      });

      // check if the the topmost window is fullscreen
      if (
        windows.length > 0 &&
        windows[windows.length - 1].get_meta_window()!.is_fullscreen()
      ) {
        panelStatus = PanelStatus.HIDDEN;
      }
    }

    if (this._panelStatus !== panelStatus) {
      this._panelStatus = panelStatus;
      this.emit('status-changed', this._panelStatus);
    }
  }

  private getHandledWindowsOnPanelMonitor(): Meta.WindowActor[] {
    return global
      .get_window_actors()
      .filter(
        wa =>
          this.handledWindow(wa) && this.isOnPanelMonitorAndCurrentWorkspace(wa)
      );
  }

  private isOnPanelMonitorAndCurrentWorkspace(wa: Meta.WindowActor): boolean {
    const metaWindow = wa.get_meta_window();

    if (!metaWindow) {
      return false;
    }

    const currentWorkspace =
      global.workspaceManager.get_active_workspace_index();
    const workspace = metaWindow.get_workspace();
    const workspaceIndex = workspace.index();

    if (
      metaWindow.is_on_primary_monitor() &&
      currentWorkspace === workspaceIndex &&
      metaWindow.showing_on_its_workspace()
    ) {
      return true;
    } else {
      return false;
    }
  }

  // Filter windows by type
  // inspired by Opacify@gnome-shell.localdomain.pl
  private handledWindow(wa: Meta.WindowActor): boolean {
    const metaWindow = wa.get_meta_window();

    if (!metaWindow) {
      return false;
    }

    const wtype = metaWindow.get_window_type();
    for (const hwtype of handledWindowTypes) {
      if (hwtype === wtype) {
        return true;
      } else if (hwtype > wtype) {
        return false;
      }
    }
    return false;
  }

  public destroy(): void {
    // disconnect our subscribers
    this.disconnectAll();

    // Disconnect global signals
    this.signalsHandler.destroy();

    // Remove residual windows signals
    for (const wa of this.trackedWindows.keys()) {
      this.removeWindowSignals(wa);
    }
    this.trackedWindows.clear();

    if (this.checkOverlapTimeoutId > 0) {
      GLib.source_remove(this.checkOverlapTimeoutId);
      this.checkOverlapTimeoutId = 0;
    }
  }
}
