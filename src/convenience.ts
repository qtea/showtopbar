/**
 * This file is part of Show Top Bar
 *
 * Copyright (C) 2020 Thomas Vogt
 * Copyright (C) 2024 tea☆
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import GObject from 'gi://GObject';

export const DEBUG = (message: string) => {
  // Enable for debugging purposes.
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  if (<any>false) {
    console.log(Date().substr(16, 8) + ' [showtopbar]: ' + message);
  }
};

type SignalObject = GObject.Object;
// connect callback on GObject has "...any => any"
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type SignalCallback = (...args: any[]) => any;

interface Signal {
  object: SignalObject;
  id: number;
}

/**
 * Try to simplify global signals handling.
 */
export class GlobalSignalsHandler {
  private signals: Signal[];

  constructor() {
    this.signals = [];
  }

  public add(object: SignalObject, event: string, callback: SignalCallback) {
    const id = object.connect(event, callback);
    this.signals.push({object, id});
  }

  public destroy() {
    this.disconnectAll();
  }

  private disconnectAll() {
    for (const signal of this.signals) {
      signal.object.disconnect(signal.id);
    }
    this.signals = [];
  }
}
