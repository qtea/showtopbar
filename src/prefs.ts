/**
 * This file is part of Show Top Bar
 *
 * Copyright (C) 2020 Thomas Vogt
 * Copyright (C) 2024 tea☆
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Adw from 'gi://Adw';
import Gtk from 'gi://Gtk';
import {ExtensionPreferences} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';

export default class ShowtopbarPreferences extends ExtensionPreferences {
  async fillPreferencesWindow(window: Adw.PreferencesWindow): Promise<void> {
    const settings = this.getSettings();

    const frame = new Gtk.ScrolledWindow({
      hscrollbarPolicy: Gtk.PolicyType.NEVER,
    });
    const builder = new Gtk.Builder();
    builder.set_translation_domain('showtopbar');
    builder.add_from_file(this.path + '/Settings.ui');

    const notebook = builder.get_object('settings_notebook') as Gtk.Notebook;
    frame.set_child(notebook);

    /*************************
     *** Section Animation ***
     *************************/
    ['animation-time-autohide'].forEach(s => {
      const settings_spin = builder.get_object(
        'spin_' + s.replace(/-/g, '_')
      ) as Gtk.SpinButton;
      settings_spin.set_value(settings.get_double(s));
      settings_spin.connect('value-changed', w => {
        settings.set_double(s, w.get_value());
      });
      settings.connect('changed::' + s, (k, b) => {
        settings_spin.set_value(settings.get_double(b));
      });
    });

    const group = new Adw.PreferencesGroup();
    group.add(frame);

    const page = new Adw.PreferencesPage();
    page.add(group);

    window.add(page);
  }
}
